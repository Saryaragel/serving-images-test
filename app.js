const express = require('express');
const app = express();
const port = 3000;
const nunjucks = require('nunjucks');
const multer = require('multer');
const upload = multer({ dest: 'uploads' });
const path = require('path'); //esta línea no estoy segura de si es necesaria
const serveStatic = require('serve-static')

app.listen(port);
app.use(express.urlencoded({ extended: true }));
app.use(express.static('uploads'));
app.use(serveStatic('uploads'));

nunjucks.configure('views', {
    autoescape: true,
    express: app
});

const mysql = require('mysql');
const connection = mysql.createConnection({
    host: 'localhost',
    database: 'files',
    user: 'files',
    password: 'files'
});

connection.connect(function (err) {
    if (err) {
        console.error('Error de conexión:' + err);
        return;
    }
    console.log('Listening on' `${port}`);
});

app.get('/', (_, res) => {
    res.render('formulario.html');
});

app.post('/', upload.single('imagen'), (req, res) => {
    console.log(req.file);
    connection.query('INSERT INTO files SET ?', { files: req.file.path }, (err, _) => { //cuando insertar datos, crear objeto con nombre fila, q siempre se me olvida!!
        if (err) throw err;
        res.send(`${req.file.path}`);
    });
});

